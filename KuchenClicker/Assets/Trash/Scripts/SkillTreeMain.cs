﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace SkillTree
{

    public class SkillTreeMain : MonoBehaviour
{

    public GameObject skillTreeSecondaryGrid;
    public GameObject skillTreeAnchor;
    private  GameObject[] secondaryGrids;
    private  GameObject[] anchors;

    public int skillTreeSecondaryGrids;
    public int skillTreeAnchors;

    public static GameObject[] Branch1;
    public static GameObject[] Branch2;
    public static GameObject[] Branch3;

        public GameObject SkillsRef;

        void Start()
    {

            CreateGrid();

    }

    void CreateGrid()
    {

        for (int i = 0; i < skillTreeSecondaryGrids; i++)
        {
            GameObject SkillTreeSecondaryGrid = (GameObject)Instantiate(skillTreeSecondaryGrid, this.transform);
            SkillTreeSecondaryGrid.name = "SkillSecondaryGrid_" + (i + 1);
        }

        secondaryGrids = GameObject.FindGameObjectsWithTag(SkillTags.SecGrid);
        anchors = GameObject.FindGameObjectsWithTag(SkillTags.Anchor);

                for (int j = 0; j < skillTreeAnchors; j++)
                {
                    GameObject SkillTreeAnchor = Instantiate(skillTreeAnchor, secondaryGrids[0].transform);
                    SkillTreeAnchor.name = "SkillTreeAnchor_" + (skillTreeAnchors - j);
                    SkillTreeAnchor.tag = SkillTags.Branch1;
                }

                for (int j = 0; j < skillTreeAnchors; j++)
                {
                    GameObject SkillTreeAnchor = Instantiate(skillTreeAnchor, secondaryGrids[1].transform);
                    SkillTreeAnchor.name = "SkillTreeAnchor_" + (skillTreeAnchors - j);
                    SkillTreeAnchor.tag = SkillTags.Branch2;
                }

                for (int j = 0; j < skillTreeAnchors; j++)
                {
                    GameObject SkillTreeAnchor = Instantiate(skillTreeAnchor, secondaryGrids[2].transform);
                    SkillTreeAnchor.name = "SkillTreeAnchor_" + (skillTreeAnchors - j);
                    SkillTreeAnchor.tag = SkillTags.Branch3;
                }

        GenerateSkills();

    }

        void GenerateSkills()
        {

            Branch1 = GameObject.FindGameObjectsWithTag(SkillTags.Branch1);
            Branch2 = GameObject.FindGameObjectsWithTag(SkillTags.Branch2);
            Branch3 = GameObject.FindGameObjectsWithTag(SkillTags.Branch3);

            SkillsRef.GetComponent<SkillTreeSkills>().generateSkillsAtAnchors();

        }
    }

}


