﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SkillTree
{

    public static class SkillTags
    {
        public static string SecGrid = "SecGrid";
        public static string Anchor = "Anchor";
        public static string Branch1 = "Branch1";
        public static string Branch2 = "Branch2";
        public static string Branch3 = "Branch3";
        public static string MainAnchor = "MainAnchor";
        public static string SkillsManager = "SkillsManager";

    }
}
