﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SkillTree
{

    public class SkillTreeSkills : MonoBehaviour
    {
        public  GameObject[] SkillsOnBranch1;
        public  GameObject[] SkillsOnBranch2;
        public  GameObject[] SkillsOnBranch3;

        public static GameObject[] SkillsOnBranch1Static;
        public static GameObject[] SkillsOnBranch2Static;
        public static GameObject[] SkillsOnBranch3Static;

        public GameObject SkillMain;

        public GameObject SkillsManager;


        public void generateSkillsAtAnchors()
        {

            for (int i = 0; i< SkillsOnBranch1.Length; i++)
            {

                Instantiate(SkillsOnBranch1[i], SkillTreeMain.Branch1[(SkillsOnBranch1.Length-1) - i].transform);

            }

            for (int j = 0; j < SkillsOnBranch2.Length; j++)
            {

                Instantiate(SkillsOnBranch2[j], SkillTreeMain.Branch2[(SkillsOnBranch2.Length-1) - j].transform);

            }

            for (int k = 0; k < SkillsOnBranch3.Length; k++)
            {

                Instantiate(SkillsOnBranch3[k], SkillTreeMain.Branch3[(SkillsOnBranch3.Length-1) - k].transform);

            }

            Instantiate(SkillMain, GameObject.FindGameObjectWithTag(SkillTags.MainAnchor).transform);

            SkillsManager.GetComponent<SkillAvailability>().GetSkills();

        }

    }

}
