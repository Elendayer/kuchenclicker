﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SkillTree
{

    public class SkillAvailability : MonoBehaviour
    {

        private  GameObject[] SkillsOnBranch1;
        private GameObject[] SkillsOnBranch2;
        private GameObject[] SkillsOnBranch3;

        private GameObject MainAnchor;


        public void GetSkills()
        {
            MainAnchor = GameObject.FindGameObjectWithTag(SkillTags.MainAnchor);

            SkillsOnBranch1 = GameObject.FindGameObjectsWithTag(SkillTags.Branch1);
            SkillsOnBranch2 = GameObject.FindGameObjectsWithTag(SkillTags.Branch2);
            SkillsOnBranch3 = GameObject.FindGameObjectsWithTag(SkillTags.Branch3);

            for (int i = 0; i < SkillsOnBranch1.Length; i++)
            { 

                SkillsOnBranch1[i].transform.GetChild(0).GetComponent<Button>().interactable = false;
                SkillsOnBranch2[i].transform.GetChild(0).GetComponent<Button>().interactable = false;
                SkillsOnBranch3[i].transform.GetChild(0).GetComponent<Button>().interactable = false;

            }
        }


        public void RefreshSkillAvailability()
        {
            if (MainAnchor.transform.GetChild(1).GetComponent<SkillExecutor>() is DamageIncreaseSkill)
            {
                Debug.Log("This is damage increase");
            }
            if (MainAnchor.transform.GetChild(1).GetComponent<SkillExecutor>().isTaken == true)
            {

                SkillsOnBranch1[0].GetComponent<Button>().interactable = true;
                SkillsOnBranch2[0].GetComponent<Button>().interactable = true;
                SkillsOnBranch3[0].GetComponent<Button>().interactable = true;

            }

            for (int i = 0; i < SkillsOnBranch1.Length; i++)

            SkillsOnBranch1[i - 1].GetComponent<Button>().interactable = true;

        }

    }
}
