﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Stats", menuName = "Stats")]
public class StatScriptObj : ScriptableObject
{
    [Header("Money")]
    public int Points;
    public int Currency;
    public float CurrencyGainMulti;
    public float CurrencyGainMultiAdd;
    public float CurrencyGainMultiBase;

    public int UpgradeCost;
    public int UpgradeCostBase;

    [Header("Damage")]
    public int Damage;
    public int DamageBase;
    public int DamageAdd;
    public int DamageMulti;
    public int DamageUpgradeCount;

    [Header("Overheat")]
    public float OverheatDecay;
    public float OverheatDecayBase;
    public float OverheatDeacyAdd;
    public float OverheatDecayMulti;
    public int OverheatMax;
    public int OverheatMaxBase;
    public int OverheatMaxAdd;
    public int OverheatMaxMulti;
    public float OverheatValue;

    [Header ("Cake")]
    public int CakeHealth;
    public int CakeHealthMaxBase;
    public int CakeHealthMax;
    public int CakeHealthAdd;
    public int CakeLevel;

    [Header("Upgrades")]
    public int UpgradeCountTotal;

    public int Skill1UpgradeCount;
    public int Skill2UpgradeCount;
    public int Skill3UpgradeCount;
    public int Skill4UpgradeCount;
    public int Skill5UpgradeCount;
    public int Skill6UpgradeCount;
    public int Skill7UpgradeCount;
    public int Skill8UpgradeCount;
    public int Skill9UpgradeCount;
}
