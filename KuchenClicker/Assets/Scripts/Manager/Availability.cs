﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SkillTree
{

    public class Availability : MonoBehaviour
    {
        public GameObject[] Skills;

        public StatScriptObj Stat;

        [SerializeField]
        static GameObject GameManager;

        public void InitAvailability()
        {
            for (int i = 0; i < Skills.Length; i++)
            {
                if (Skills[i].GetComponent<SkillExecutor>().isRoot == false)
                {
                    Skills[i].GetComponent<Button>().interactable = false;
                }
            }
        }

        public void RefreshSkillAvailability(GameObject Next, GameObject Self, SkillExecutor Script)
        {
            if (Script.SkillLevel == Script.SkillLevelMax)
            {
                Debug.Log(Script.SkillLevel);

                Script.isTaken = true;
                Script.Button.interactable = false;

                if (Script.isLast == false)
                {
                    Next.GetComponent<Button>().interactable = true;
                }
            }
        }

        public void RefreshBuyability()
        {
            Stat.UpgradeCost = Stat.UpgradeCostBase * (1 + Stat.UpgradeCountTotal);

            if (Stat.Currency <= Stat.UpgradeCost)
            {
                for (int i = 0; i < Skills.Length; i++)
                {
                    Skills[i].GetComponent<Button>().interactable = false;
                }
            }
            if (Stat.Currency >= Stat.UpgradeCost)
            {
                for (int i = 0; i < Skills.Length; i++)
                {
                    Debug.Log(Skills[i].GetComponent<SkillExecutor>().PreviousSkill.GetComponent<SkillExecutor>().name);
                    Debug.Log(Skills[i].GetComponent<SkillExecutor>().PreviousSkill.GetComponent<SkillExecutor>().isTaken);
                    if (Skills[i].GetComponent<SkillExecutor>().PreviousSkill.GetComponent<SkillExecutor>().isTaken == true && Skills[i].GetComponent<SkillExecutor>().SkillLevel != 5)
                    {
                        Debug.Log(Skills[i]);
                        Skills[i].GetComponent<Button>().interactable = true;
                    }
                }
            }
        }

        public void RefreshUpgradeCount()
        {
            Stat.Skill1UpgradeCount = Skills[0].GetComponent<SkillExecutor>().SkillLevel;
            Stat.Skill2UpgradeCount = Skills[1].GetComponent<SkillExecutor>().SkillLevel;
            Stat.Skill3UpgradeCount = Skills[2].GetComponent<SkillExecutor>().SkillLevel;
            Stat.Skill4UpgradeCount = Skills[3].GetComponent<SkillExecutor>().SkillLevel;
            Stat.Skill5UpgradeCount = Skills[4].GetComponent<SkillExecutor>().SkillLevel;
            Stat.Skill6UpgradeCount = Skills[5].GetComponent<SkillExecutor>().SkillLevel;

            RefreshUpgradeCountMax();
        }

        public void RefreshUpgradeCountMax()
        {
            Stat.UpgradeCountTotal = (Stat.Skill1UpgradeCount + Stat.Skill2UpgradeCount + Stat.Skill3UpgradeCount + Stat.Skill4UpgradeCount + Stat.Skill5UpgradeCount + Stat.Skill6UpgradeCount);
        }
    }
}
