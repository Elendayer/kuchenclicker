﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SkillTree
{
    public class InitGame : MonoBehaviour
    {
        public GameObject GameManager;

        public StatScriptObj Stat;

        public GameObject Counter;

        [SerializeField]
        private GameObject[] Skills;

        void Start()
        {
            Skills = GameManager.GetComponent<Availability>().Skills; 

            //GameManager.GetComponent<Player>().Load();

            GameManager.GetComponent<Availability>().Skills[0].GetComponent<SkillExecutor>().SkillLevel = Stat.Skill1UpgradeCount;
            GameManager.GetComponent<Availability>().Skills[1].GetComponent<SkillExecutor>().SkillLevel = Stat.Skill2UpgradeCount;
            GameManager.GetComponent<Availability>().Skills[2].GetComponent<SkillExecutor>().SkillLevel = Stat.Skill3UpgradeCount;
            GameManager.GetComponent<Availability>().Skills[3].GetComponent<SkillExecutor>().SkillLevel = Stat.Skill4UpgradeCount;
            GameManager.GetComponent<Availability>().Skills[4].GetComponent<SkillExecutor>().SkillLevel = Stat.Skill5UpgradeCount;
            GameManager.GetComponent<Availability>().Skills[5].GetComponent<SkillExecutor>().SkillLevel = Stat.Skill6UpgradeCount;

            InitGameInStart();
        }

        private void InitGameInStart()
        {
            for (int i = 0; i < Skills.Length; i++)
            {
                Skills[i].GetComponent<SkillExecutor>().UpdateTheTracker();
            }

            GameManager.GetComponent<Availability>().InitAvailability();
            GameManager.GetComponent<Availability>().RefreshBuyability();
            GameManager.GetComponent<Availability>().RefreshUpgradeCountMax();
        }
    }
}
