﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace SkillTree
{
    public class UpgradeTracker : MonoBehaviour
    {
        public GameObject[] UpgradePips;

        public void UpdatePips(int j)
        {
            int PipLevel = j;
            for (int i = 1; i < j+1; i++)
            {
                UpgradePips[PipLevel - i].GetComponent<Image>().color = new Color(255, 0, 0, 255);
            }
        }
    }
}

