﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SkillTree
{
    public class DamageIncreaseSkill : SkillExecutor
    {
        public void ToExcute()
        {
            stat.Damage = stat.Damage + ExecuteSkillIntAdd(stat.DamageAdd, stat.DamageBase);
        }
    }
}
