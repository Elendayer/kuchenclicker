﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SkillTree
{
    public class DamageMultiSkill : SkillExecutor
    {
        public void ToExcute()
        {
            stat.Damage = stat.Damage + ExecuteSkillIntMulti(stat.DamageMulti, stat.DamageBase);
        }
    }
}

