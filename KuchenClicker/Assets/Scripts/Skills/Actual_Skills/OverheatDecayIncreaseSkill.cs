﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SkillTree
{
    public class OverheatDecayIncreaseSkill : SkillExecutor
    {
        public void ToExcute()
        {
            stat.OverheatDecay = stat.OverheatDecay + ExecuteSkillFloatAdd(stat.OverheatDeacyAdd, stat.OverheatDecayBase);
        }
    }
}
