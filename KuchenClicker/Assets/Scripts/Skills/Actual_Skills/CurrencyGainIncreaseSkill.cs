﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace SkillTree
{
    public class CurrencyGainIncreaseSkill : SkillExecutor
    {
        public void ToExcute()
        {
            stat.CurrencyGainMulti = stat.CurrencyGainMultiBase + ExecuteSkillFloatAdd(stat.CurrencyGainMulti, stat.CurrencyGainMultiAdd);
        }
    }
}

