﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SkillTree
{
    public class OverheatDecayMultiSkill : SkillExecutor
    {
        public void ToExcute()
        {
            stat.OverheatDecay = stat.OverheatDecay + ExecuteSkillFloatMulti(stat.OverheatDecayMulti, stat.OverheatDecayBase);
        }
    }
}
