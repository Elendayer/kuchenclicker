﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SkillTree
{
    public class OverheatMaxIncreaseSkill1 : SkillExecutor
    {
        public void ToExcute()
        {
            stat.OverheatMax = stat.OverheatMax + ExecuteSkillIntAdd(stat.OverheatMaxAdd, stat.OverheatMaxBase);
        }
    }
}