﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace SkillTree
{

    public class SkillExecutor : MonoBehaviour
    {
        public bool isRoot;

        [SerializeField]
        private GameObject nextSkill;

        [SerializeField]
        private GameObject previousSkill;

        public GameObject UpgradeTracker;

        private int skillLevel;
        private int skillLevelMax = 5;

        private Button button;

        public bool isTaken;

        public bool isLast;

        public StatScriptObj stat;

        static GameObject GameManager;

        private TextMeshProUGUI Text;

        public int SkillLevel
        {
            get => skillLevel;
            set => skillLevel = value;
        }

        public int SkillLevelMax
        {
            get => skillLevelMax;
            set => skillLevelMax = value;
        }

        public Button Button
        {
            get => button;
            set => button = value;
        }

        public GameObject Next
        {
            get => nextSkill;
            set => nextSkill = value;
        }

        public GameObject PreviousSkill
        {
            get => previousSkill;
            set => previousSkill = value;
        }

        private void Start()
        {
            isTaken = false;

            Button = GetComponent<Button>();

            GameManager = GameObject.FindGameObjectWithTag(SkillTags.SkillsManager);

            AddToExecute();

            //Tmp Text Gen
            Text = this.gameObject.GetComponentInChildren<TextMeshProUGUI>();
            Text.text = gameObject.name.ToString();

        }

        public void UpdateTheTracker()
        {
                UpgradeTracker.GetComponent<UpgradeTracker>().UpdatePips(SkillLevel);
        }

        void AddToExecute()
        {

        }

        public void IncreaseSkill()
        {
            SkillLevel++;

            // Visual Update how many upgrades are taken
            UpgradeTracker.GetComponent<UpgradeTracker>().UpdatePips(SkillLevel);

            //Paying up
            stat.Currency = stat.Currency - stat.UpgradeCost;

            GameManager.GetComponent<Availability>().RefreshSkillAvailability(nextSkill, this.gameObject, this);
            GameManager.GetComponent<Availability>().RefreshUpgradeCount();
            GameManager.GetComponent<Availability>().RefreshBuyability();      
        }

        public int ExecuteSkillIntAdd(int multiplyer, int baseValue)
        {
            if (SkillLevel < skillLevelMax)
            {
                    if (isTaken == false)
                    {
                        baseValue = multiplyer + baseValue;
                        IncreaseSkill();              
                    }
            }

            return baseValue;
        }

        public float ExecuteSkillFloatAdd(float multiplyer, float baseValue)
        {
            if (SkillLevel < skillLevelMax)
            {
                    if (isTaken == false)
                    {

                        baseValue = multiplyer + baseValue;
                        IncreaseSkill();

                    }
            }

            return baseValue;
        }

        public int ExecuteSkillIntMulti(int multiplyer, int baseValue)
        {
            if (SkillLevel < skillLevelMax)
            {
                if (isTaken == false)
                {
                    baseValue = multiplyer * baseValue;
                    IncreaseSkill();
                }
            }

            return baseValue;
        }

        public float ExecuteSkillFloatMulti(float multiplyer, float baseValue)
        {
            if (SkillLevel < skillLevelMax)
            {
                if (isTaken == false)
                {

                    baseValue = multiplyer * baseValue;
                    IncreaseSkill();

                }
            }

            return baseValue;
        }

        public void ExecuteSkillProccInt(int multiplyer, int baseValue, float timer)
        {

        }

        public void ExecuteSkillProccFloat(float multiplyer, float baseValue, float timer)
        {

        }
    }
}
