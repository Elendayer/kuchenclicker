﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Overheat : MonoBehaviour
{

    public StatScriptObj Stat;

    public bool Lockout = false;

    [SerializeField]
    private float lockoutTimeMax = 300f;
    [SerializeField]
    private float lockoutTime;

    private Slider OverheatSlider;

    public Image SliderColor;

    private void Start()
    {
        OverheatSlider = this.gameObject.GetComponent<Slider>();

        SliderColor = OverheatSlider.gameObject.transform.Find("Fill Area").Find("Fill").GetComponent<Image>();          
    }

    void Update()
    {
        if (Stat.OverheatValue > 0)
        {
            OverheatDecayUpdate();
        }

        if (Stat.OverheatValue > Stat.OverheatMax)
        {
            Lockout = true;
            SliderColor.color = new Color(255, 0, 0, 255);

        }

        if (Stat.OverheatValue < Stat.OverheatMax)
        {
            Lockout = false;
            SliderColor.color = new Color(0, 255, 0, 255);
        }

        SliderValue();

    }

    private void OverheatDecayUpdate()
    {
        Stat.OverheatValue = Stat.OverheatValue - ((Stat.OverheatDecay + Stat.OverheatDeacyAdd) * Time.deltaTime);
    }

    private void SliderValue()
    {
        OverheatSlider.value = Stat.OverheatValue;
        OverheatSlider.maxValue = Stat.OverheatMax;
    }
}
