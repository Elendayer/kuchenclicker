﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SceneButtons : MonoBehaviour
{

    private string Menu = "Menu";
    private string Game = "Game";
    private string SkillTree = "SkillTree";

    public void BackToMenu()
    {
        SceneManager.LoadScene(Menu);
    }

    public void ToGame()
    {
        SceneManager.LoadScene(Game);
    }

    public void BackToGame()
    {
        SceneManager.UnloadSceneAsync(SkillTree);
    }

    public void ToUpgrades()
    {
        SceneManager.LoadScene(SkillTree);
    }

}
