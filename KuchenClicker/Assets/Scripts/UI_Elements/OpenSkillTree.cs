﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenSkillTree : MonoBehaviour
{
    [SerializeField]
    private bool isOpen = false;

    private Vector2 Start = new Vector2 (0,0);
    private Vector2 End = new Vector2(1080, 0);

    [SerializeField]
    private GameObject Skilltreemover;

    public void OpenAndClose()
    {

        //Open
        if (isOpen == false)
        {

            Skilltreemover.transform.position = Start;
            isOpen = true;
            return;
        }

        // close
        if (isOpen == true)
        {

            Skilltreemover.transform.position = End;
            isOpen = false;
           
        }
    }
}
