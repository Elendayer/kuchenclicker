﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SkillTree
{
    public class MainButtonClick : MonoBehaviour
    {

        public StatScriptObj Stat;

        public GameObject CounterText;

        public GameObject OverheatMeter;

        public GameObject GameManager;

        private void Start()
        {
            CounterText.GetComponent<PointCounter>().CounterUpdate(Stat.Points, Stat.Currency, Stat.UpgradeCost);
        }

        public void DealDamage()
        {
            if (OverheatMeter.GetComponent<Overheat>().Lockout == false)
            {
                Stat.Points = Stat.Points + Stat.Damage;
                Stat.Currency = Mathf.RoundToInt(Stat.Currency + Stat.Damage * Stat.CurrencyGainMulti);
                Stat.CakeHealth -= Stat.Damage;
                Stat.OverheatValue = Stat.OverheatValue + Stat.Damage;
            }

            GetComponent<Kuchen>().HealthCalc();

            GameManager.GetComponent<Availability>().RefreshBuyability();

            CounterText.GetComponent<PointCounter>().CounterUpdate(Stat.Points, Stat.Currency, Stat.UpgradeCost);

            GameManager.GetComponent<Player>().Save();
        }
    }
}
