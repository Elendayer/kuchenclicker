﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class PointCounter : MonoBehaviour
{
    [SerializeField]
    private TextMeshProUGUI Text1;
    [SerializeField]
    private TextMeshProUGUI Text2;
    [SerializeField]
    private TextMeshProUGUI Text3;


    public void CounterUpdate(int Points, int Currency, int UpgradeCost)
    {
        Text1.text = "Points: " + Points;
        Text2.text = "Kalories: " + Currency;
        Text3.text = "Upgrade Cost: " + UpgradeCost;
    }
}
