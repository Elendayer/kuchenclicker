﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LookoutCross : MonoBehaviour
{
    public GameObject overheatMeter;

    public Image image;

    void Update()
    {
        if (overheatMeter.GetComponent<Overheat>().Lockout == true)
        {
            image.enabled = true;
        }
        else
        {
            image.enabled = false;
        }
    }
}
