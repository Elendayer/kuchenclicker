﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using SimpleJSON;

public class Player : MonoBehaviour
{
    public StatScriptObj Stat;

    public void Save()
    {
        JSONObject playerJson = new JSONObject();

        //Money
        playerJson.Add("Points", Stat.Points);
        playerJson.Add("Currency", Stat.Currency);
        playerJson.Add("CurrencyGainMulti", Stat.CurrencyGainMulti);
        playerJson.Add("CurrencyGainMultiAdd", Stat.CurrencyGainMultiAdd);
        playerJson.Add("CurrencyGainMultiBase", Stat.CurrencyGainMultiBase);

        playerJson.Add("UpgradeCost", Stat.UpgradeCost);
        playerJson.Add("UpgradeCostBase", Stat.UpgradeCostBase);

        //Damage
        playerJson.Add("Damage", Stat.Damage);
        playerJson.Add("DamageBase", Stat.DamageBase);
        playerJson.Add("DamageAdd", Stat.DamageAdd);
        playerJson.Add("DamageMulti", Stat.DamageMulti);
        playerJson.Add("DamageUpgradeCount", Stat.DamageUpgradeCount);

        //Overheat
        playerJson.Add("OverheatDecay", Stat.OverheatDecay);
        playerJson.Add("OverheatDecayBase", Stat.OverheatDecayBase);
        playerJson.Add("OverheatDeacyAdd", Stat.OverheatDeacyAdd);
        playerJson.Add("OverheatDecayMulti", Stat.OverheatDecayMulti);
        playerJson.Add("OverheatMax", Stat.OverheatMax);
        playerJson.Add("OverheatMaxBase", Stat.OverheatMaxBase);
        playerJson.Add("OverheatMaxAdd", Stat.OverheatMaxAdd);
        playerJson.Add("OverheatMaxMulti", Stat.OverheatMaxMulti);
        playerJson.Add("OverheatValue", Stat.OverheatValue);

        //Cake
        playerJson.Add("CakeHealth", Stat.CakeHealth);
        playerJson.Add("CakeHealthMaxBase", Stat.CakeHealthMaxBase);
        playerJson.Add("CakeHealthMax", Stat.CakeHealthMax);
        playerJson.Add("CakeHealthAdd", Stat.CakeHealthAdd);
        playerJson.Add("CakeLevel", Stat.CakeLevel);

        //Upgrades
        playerJson.Add("UpgradeCountTotal", Stat.UpgradeCountTotal);

        playerJson.Add("Skill1UpgradeCount", Stat.Skill1UpgradeCount);
        playerJson.Add("Skill2UpgradeCount", Stat.Skill2UpgradeCount);
        playerJson.Add("Skill3UpgradeCount", Stat.Skill3UpgradeCount);
        playerJson.Add("Skill4UpgradeCount", Stat.Skill4UpgradeCount);
        playerJson.Add("Skill5UpgradeCount", Stat.Skill5UpgradeCount);
        playerJson.Add("Skill6UpgradeCount", Stat.Skill6UpgradeCount);
        playerJson.Add("Skill7UpgradeCount", Stat.Skill7UpgradeCount);
        playerJson.Add("Skill8UpgradeCount", Stat.Skill8UpgradeCount);
        playerJson.Add("Skill9UpgradeCount", Stat.Skill9UpgradeCount);

        //SAVE JSON IN COMPUTER
        string path = Application.persistentDataPath + "/PlayerSave.json";
        File.WriteAllText(path, playerJson.ToString());

    }

    public void Load()
    {
        string path = Application.persistentDataPath + "/PlayerSave.json";
        string jsonString = File.ReadAllText(path);
        JSONObject playerJson = (JSONObject)JSON.Parse(jsonString);
        //SET VALUES
        //Money
        Stat.Points = playerJson["Points"];
        Stat.Currency = playerJson["Currency"];
        Stat.CurrencyGainMulti = playerJson["CurrencyGainMulti"];
        Stat.CurrencyGainMultiAdd = playerJson["CurrencyGainMultiAdd"];
        Stat.CurrencyGainMultiBase = playerJson["CurrencyGainMultiBase"];

        Stat.UpgradeCost = playerJson["UpgradeCost"];
        Stat.UpgradeCostBase = playerJson["UpgradeCostBase"];

        //Damage
        Stat.Damage = playerJson["Damage"];
        Stat.DamageBase = playerJson["DamageBase"];
        Stat.DamageAdd = playerJson["DamageAdd"];
        Stat.DamageMulti = playerJson["DamageMulti"];
        Stat.DamageUpgradeCount = playerJson["DamageUpgradeCount"];

        //Overheat
        Stat.OverheatDecay = playerJson["OverheatDecay"];
        Stat.OverheatDecayBase = playerJson["OverheatDecayBase"];
        Stat.OverheatDeacyAdd = playerJson["OverheatDeacyAdd"];
        Stat.OverheatMax = playerJson["OverheatMax"];
        Stat.OverheatMaxBase = playerJson["OverheatMaxBase"];
        Stat.OverheatMaxAdd = playerJson["OverheatMaxAdd"];
        Stat.OverheatValue = playerJson["OverheatValue"];

        //Cake
        Stat.CakeHealth = playerJson["CakeHealth"];
        Stat.CakeHealthMaxBase = playerJson["CakeHealthMaxBase"];
        Stat.CakeHealthMax = playerJson["CakeHealthMax"];
        Stat.CakeHealthAdd = playerJson["CakeHealthAdd"];
        Stat.CakeLevel = playerJson["CakeLevel"];

        //Upgrades
        Stat.UpgradeCountTotal = playerJson["UpgradeCountTotal"];

        Stat.Skill1UpgradeCount = playerJson["Skill1UpgradeCount"];
        Stat.Skill2UpgradeCount = playerJson["Skill2UpgradeCount"];
        Stat.Skill3UpgradeCount = playerJson["Skill3UpgradeCount"];
        Stat.Skill4UpgradeCount = playerJson["Skill4UpgradeCount"];
        Stat.Skill5UpgradeCount = playerJson["Skill5UpgradeCount"];
        Stat.Skill6UpgradeCount = playerJson["Skill6UpgradeCount"];
        Stat.Skill7UpgradeCount = playerJson["Skill7UpgradeCount"];
        Stat.Skill8UpgradeCount = playerJson["Skill8UpgradeCount"];
        Stat.Skill9UpgradeCount = playerJson["Skill9UpgradeCount"];

    }
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.S)) Save();
    }
}